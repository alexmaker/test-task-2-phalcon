<?php

use Phalcon\Db\Column as Column;
use Phalcon\Db\Index as Index;
use Phalcon\Db\Reference as Reference;
use Phalcon\Mvc\Model\Migration;

class PhonesMigration_100 extends Migration
{
    public function up()
    {
        self::$connection->createTable(
            'phones',
            null,
            [
                'columns' => [
                    new Column(
                        'id',
                        [
                            'type'          => Column::TYPE_INTEGER,
                            'size'          => 10,
                            'notNull'       => true,
                            'autoIncrement' => true,
                            'primary'       => true,
                        ]
                    ),
                    new Column(
                        'first_name',
                        [
                            'type'    => Column::TYPE_VARCHAR,
                            'size'    => 255,
                            'notNull' => true,
                        ]
                    ),
                    new Column(
                        'last_name',
                        [
                            'type'    => Column::TYPE_VARCHAR,
                            'size'    => 255,
                            'notNull' => false,
                        ]
                    ),
                    new Column(
                        'phone',
                        [
                            'type'    => Column::TYPE_CHAR,
                            'size'    => 20,
                            'notNull' => true,
                            ''
                        ]
                    ),
                    new Column(
                        'country_code',
                        [
                            'type'    => Column::TYPE_CHAR,
                            'size'    => 3,
                            'notNull' => false,
                        ]
                    ),
                    new Column(
                        'timezone',
                        [
                            'type'    => Column::TYPE_VARCHAR,
                            'size'    => 50,
                            'notNull' => false,
                        ]
                    ),
                    new Column(
                        'inserted',
                        [
                            'type'    => Column::TYPE_DATETIME,
                            'notNull' => true,
                        ]
                    ),
                    new Column(
                        'updated',
                        [
                            'type'    => Column::TYPE_DATETIME,
                            'notNull' => true,
                        ]
                    ),
                ],
                'indexes' => [
                    new Index('phone_uniq', ['phone'], 'UNIQUE')
                ]
            ]
        );
    }

    public function down()
    {
        self::$connection->dropTable('phones');
    }
}