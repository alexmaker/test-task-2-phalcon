<?php


namespace Rest\Phonebook\Controllers;

use Exception;
use Phalcon\Mvc\Controller;
use Rest\Phonebook\Models\Phones;
use Phalcon\Http\Response;
use Phalcon\Paginator\Adapter\Model as PaginatorModel;

class PhonesController extends Controller
{
    public function getPhone(int $id) : Response
    {
        $phone = Phones::findFirst($id);

        if($phone === false ) {
            return $this->makeResponse(404, "Not Found", ['status' => "Not found"]);
        }

        $data = [
            'id'   => $phone->getId(),
            'phone' => $phone->phone,
            'firstName' => $phone->first_name,
            'lastName' => $phone->last_name,
            'countryCode' => $phone->country_code,
            'timezone' => $phone->phone,
            'inserted' => $phone->getInserted(),
            'updated' => $phone->getUpdated()
        ];

        return $this->makeResponse(200, "OK", [
            'status' => 'OK',
            'data'   => $data
        ]);
    }

    public function getAllPhones(int $limit = 30, int $page = 1) : Response
    {
        $limit = (int) $this->request->getQuery('perPage') !== 0 ?: $limit;
        $page = (int) $this->request->getQuery('page') !== 0 ?: $page;

        return $this->getPhones($limit, $page);
    }

    public function getPhoneByName(string $search, int $limit = 30, int $page = 1) : Response
    {
        $limit = (int) $this->request->getQuery('perPage') !== 0 ?: $limit;
        $page = (int) $this->request->getQuery('page') !== 0 ?: $page;
        $params = [
            'conditions' => sprintf("first_name LIKE '%%%s%%' OR last_name LIKE '%%%s%%'", $search, $search)
        ];

        return $this->getPhones($limit, $page, $params);
    }

    public function getPhones(int $limit = 30, int $page = 1, array $params = []) : Response
    {
        $filter = ['order' => 'phone'] + $params;
        $phones = Phones::find($filter);
        $paginator = new PaginatorModel(
            [
                'data'  => $phones,
                'limit' => $limit,
                'page'  => $page,
            ]
        );
        $page = $paginator->getPaginate();

        $data = [];
        foreach ($page->items as $phone) {
            $data[] = [
                'id'   => $phone->getId(),
                'phone' => $phone->phone,
                'firstName' => $phone->first_name,
                'lastName' => $phone->last_name,
                'countryCode' => $phone->country_code,
                'timezone' => $phone->timezone,
                'inserted' => $phone->getInserted(),
                'updated' => $phone->getUpdated()
            ];
        }

        $body = [
            'status' => 'OK',
            'data'   => $data,
            'totalItems' => $page->total_items,
            'pagination' => [
                'totalPages' => $page->total_pages,
                'currentPage' => $page->current,
                'nextPage' => $page->next,
                'perPage' => $page->limit
            ]
        ];

        return $this->makeResponse(200, "OK", $body);
    }

    public function addPhone() : Response
    {
        $data = $this->request->getJsonRawBody();
        $phone = new Phones();

        $phone->first_name = $data->firstName;
        $phone->last_name = $data->lastName;
        $phone->country_code = strtoupper($data->countryCode);
        $phone->timezone = $data->timezone;
        $phone->setPhone($data->phone);

        // Check if the insertion was successful
        if ($phone->save() === true) {
            return $this->makeResponse(201, "Created", [
                'status' => 'OK',
                'data'   => $phone
            ]);
        } else {
            return $this->makeResponse(409, "Conflict", null, $phone->getMessages());
        }
    }

    public function updatePhone(int $id) : Response
    {
        $data = $this->request->getJsonRawBody();
        $phone = Phones::findFirst($id);

        if($phone === false ) {
            return $this->makeResponse(404, "Not Found", ['status' => "Not found"]);
        }

        $phone->first_name = $data->firstName ?? $phone->first_name;
        $phone->last_name = $data->lastName ?? $phone->last_name;
        $phone->country_code = strtoupper($data->countryCode) ?? $phone->country_code;
        $phone->timezone = $data->timezone ?? $phone->timezone;
        if($data->phone)
            $phone->setPhone($data->phone);

        // Check if the insertion was successful
        if ($phone->save() === true) {
            return $this->makeResponse(200, "OK", [
                'status' => 'Updated',
                'data'   => $phone
            ]);
        } else {
            return $this->makeResponse(409, "Conflict", null, $phone->getMessages());
        }
    }

    public function deletePhone(int $id) : Response
    {
        $phone = Phones::findFirst($id);

        if($phone === false ) {
            return $this->makeResponse(404, "Not Found", ['status' => "Not found"]);
        }

        // Check if the insertion was successful
        if ($phone->delete() === true) {
            return $this->makeResponse(200, "OK", ['status' => 'OK']);
        } else {
            return $this->makeResponse(409, "Conflict", null, $phone->getMessages());
        }
    }

    private function makeResponse(int $status, string $msg, array $content = null, array $errors = null) : Response
    {
        $response = new Response();
        $response->setStatusCode($status, $msg);

        if(!$errors) {
            $response->setJsonContent($content);
        } else {
            $messages = [];

            foreach ($errors as $error) {
                $messages[] = $error->getMessage();
            }

            $response->setJsonContent(
                [
                    'status'   => 'ERROR',
                    'messages' => $messages,
                ]
            );
        }

        return $response;
    }

    private function getCountriesFromHostaway()
    {
        return (array) json_decode(file_get_contents("https://api.hostaway.com/countries"))->result;
    }

    private function getTimezonesFromHostaway()
    {
        return (array) json_decode(file_get_contents("https://api.hostaway.com/timezones"))->result;
    }

    /*
     * Validation Functions
    */
    public function validateTimezone(string $timezone) : bool
    {
        $zones = $this->getTimezonesFromHostaway();
        if($zones[$timezone])
            return true;
        return false;
    }

    public function validateCountryCode(string $country) : bool
    {
        $countries = $this->getCountriesFromHostaway();
        if($countries[$country])
            return true;
        return false;
    }
}