<?php
/**
 * Local variables
 * @var \Phalcon\Mvc\Micro $app
 */
use Rest\Phonebook\Models\Phones;
use Phalcon\Http\Response;
use Rest\Phonebook\Controllers\PhonesController;
use Phalcon\Mvc\Micro\Collection as MicroCollection;

/**
 * Add your routes here
 */

$phones = new MicroCollection();
$phones->setHandler(new PhonesController());

$app->get('/', function () {
    echo $this['view']->render('index');
});

$phones->get('/api/phones', 'getAllPhones'); // Retrieves all phones
$phones->get('/api/phones/{id:[0-9]+}', 'getPhone'); // Retrieves robots based on primary key
$phones->get('/api/phones/search/{name}', 'getPhoneByName'); // Searches for robots with $name in their name
$phones->post('/api/phones', 'addPhone'); // Adds a new phone
$phones->put('/api/phones/{id:[0-9]+}', 'updatePhone'); // Updates robots based on primary key
$phones->delete('/api/phones/{id:[0-9]+}', 'deletePhone'); // Deletes phones based on primary key
$app->mount($phones);

/**
 * Not found handler
 */
$app->notFound(function () use($app) {
    $app->response->setStatusCode(404, "Not Found")->sendHeaders();
    echo $app['view']->render('404');
});
