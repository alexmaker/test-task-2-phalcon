<?php

namespace Rest\Phonebook\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Message;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Uniqueness;
use Phalcon\Validation\Validator\InclusionIn;
use DateTime;
use Rest\Phonebook\Controllers\PhonesController;

class Phones extends Model
{
    protected $id;

    public $first_name;
    public $last_name;
    public $country_code;
    public $timezone;

    protected $phone;

    private $inserted;
    private $updated;

    public function initialize()
    {
        $this->setSource('phones');
    }

    public function getId()
    {
        return $this->id;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function getInserted()
    {
        return (new \DateTime($this->inserted))->format("c");
    }

    public function getUpdated()
    {
        return (new \DateTime($this->updated))->format("c");
    }

    public function setPhone(string $phone)
    {
        $this->phone = "+" . preg_replace("#[^0-9]#", "", $phone);
    }

    public function setInserted(DateTime $date)
    {
        $this->inserted = $this->inserted ?? $date->format("c");
    }

    public function setUpdated(DateTime $date)
    {
        $this->updated = $date->format("c");
    }

    public function validation()
    {
        $controller = new PhonesController;
        $validator = new Validation();

        if (strlen(trim($this->first_name)) <= 0) {
            $this->appendMessage(
                new Message('First name is required')
            );
        }

        if (strlen(trim($this->phone)) <= 9) {
            $this->appendMessage(
                new Message('Incorrect phone number. Please check number size')
            );
        }

        $tz = trim($this->timezone);
        if(strlen($tz) > 0 ) {
            if($controller->validateTimezone($tz) === false) {
                $this->appendMessage(
                    new Message("{$tz} is incorrect timezone.")
                );
            }
        }

        $code = trim($this->country_code);
        if(strlen($code) > 0 ) {
            if($controller->validateCountryCode($code) === false) {
                $this->appendMessage(
                    new Message("{$code} is incorrect country code. Please fill it in international format.")
                );
            }
        }

        $validator->add(
            'phone',
            new Uniqueness(
                [
                    'field'   => 'phone',
                    'message' => 'The phone number must be unique',
                ]
            )
        );

        // Check if any messages have been produced
        if ($this->validationHasFailed() === true) {
            return false;
        }
    }

    //beforeValidationOnCreate, beforeValidationOnUpdate
    public function beforeValidation()
    {
        $date = new DateTime();
        $this->setInserted($date);
        $this->setUpdated($date);
    }
}