<?php

/**
 * Registering an autoloader
 */
$loader = new \Phalcon\Loader();

$loader->registerNamespaces(
    [
        'Rest\Phonebook\Models' => APP_PATH . '/models/',
        'Rest\Phonebook\Controllers' => APP_PATH . '/controllers/',
    ]
);

$loader->registerDirs(
    [
        $config->application->modelsDir
    ]
)->register();
