## Getting started
Clone and composer install
```bash
$ git clone git@bitbucket.org:alexmaker/hostaway-testwork.git
$ cd hostaway-testwork
$ composer install
```

Edit the config.php for database connection
```bash
$ vim ./app/config/config.php
``` 

Run migration and php web server
```bash
$ ./vendor/bin/phalcon migration run
$ cd public
$ php -S localhost:8000
``` 

## Testing the API
Get list of all phone numbers (default page=1 and perPage=30)
```bash
$ curl -i -X GET "http://localhost:8000/api/phones?page=1&perPage=30"
``` 

Search by name (default page=1 and perPage=30)
```bash
$ curl -i -X GET "http://localhost:8000/api/phones/search/{name}?page=1&perPage=30"
``` 

Get by ID
```bash
$ curl -i -X GET "http://localhost:8000/api/phones/{id}"
``` 

Delete by ID
```bash
$ curl -i -X DELETE "http://localhost:8000/api/phones/{id}"
```

Add phone number. All number convert to \+[0-9]+. Check all validation out in models/Phones.php.
```bash
$ curl -i -X POST -d '{"firstName":"Alexander","lastName":"Ivanov","phone":"+79141771313", "timezone": "Asia/Vladivostok", "countryCode": "RU"}' http://localhost:8000/api/phones
``` 

Update by ID
```bash
$ curl -i -X PUT -d '{"firstName":"Mattew", "phone":"+79141231233"}' "http://localhost:8000/api/phones/{id}"
```  

